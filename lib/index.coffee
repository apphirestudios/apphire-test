path = require 'path'
Mocha = require 'mocha'
coMocha = require 'co-mocha'
coMocha(Mocha)
global.mocha = new Mocha
  ui:"bdd",
  reporter:"spec",
  timeout:10000,
  slow:5000
  bail: true
global.describe = mocha.describe
global.should = require('chai').should()
global.expect = require('chai').expect
global.request = supertest = require "supertest"
helpers = require 'apphire-helpers'


#Monkey patching supertest to allow working as coroutine
Test = supertest.Test
oldEnd = Test::end

Test::end = ->
  if arguments.length > 0
    return oldEnd.apply(this, arguments)
  self = this
  (callback) ->
    oldEnd.call self, (err, res) ->
      # allow events handlers to run first
      process.nextTick ->
        callback err, res

global.log = console.log.bind @
global.async = require 'co'
global.cwd = path.resolve('.') + '/'

global.throws = (fn, regexpOrErrorType)-> async ->
  error = undefined
  try
    yield fn
  catch e
    error = e
  if error?
    if regexpOrErrorType
      if regexpOrErrorType instanceof RegExp
        regexp = regexpOrErrorType
        if not regexp.test error.toString()
          throw new Error('Function should throw an error matching ' + (regexp or '') + ' but it returned ' + error.toString())
      else
        errorType = regexpOrErrorType
        if not(error instanceof errorType)
          throw new Error('Function should throw an error type ' + errorType.name + ' but it returned ' + error.name + ' : ' + error.toString())

  else
    throw new Error('Function should throw an error but it ended normally')

path = require('path')
webpack = require('webpack')
koa = require('koa')
staticServer = require 'koa-static'
devMiddleware = require('koa-webpack-dev-middleware')
hotMiddleware = require('koa-webpack-hot-middleware')

makeWebpackConfig = (entry)->
  return ret =
    devtool: 'eval'
    entry: [
      require.resolve 'webpack-hot-middleware/client'
      path.join __dirname, './main'
      entry
    ]
    output:
      path: path.join(__dirname, 'dist')
      filename: 'bundle.js'
      publicPath: '/static/'
    plugins: [ new (webpack.HotModuleReplacementPlugin) ]
    resolve:
      extensions: [
        ''
        '.js'
        '.jsx'
        '.coffee'
        '.csx'
        '.json'
      ]
      fallback: path.join(__dirname, "../node_modules")
    module:
      exprContextRegExp: /$^/,
      exprContextCritical: false,
      loaders: [
        {
          test: /\.coffee$/
          loaders: [ 'coffee' ]
        }
        {
          test: /\.csx$/
          loaders: [ 'apphire-csx-loader' ]
        }
        {
          test: /\.json$/
          loaders: [ 'json' ]
        }
      ]
    node:
      fs: "empty"
      child_process: "empty"


Apphire = require 'apphire'
nodemon = require 'nodemon'


module.exports = class Test extends Apphire
  run: (modules)->
    for m in modules
      mocha.addFile require.resolve path.resolve cwd, m
    mocha.run (failures)->
      log failures
      process.exit failures

  serve: ({entry})->
    config = makeWebpackConfig(entry)
    compiler = webpack(config)
    @use devMiddleware(compiler,
      noInfo: true
      publicPath: config.output.publicPath
      historyApiFallback: true)

    @use hotMiddleware(compiler)

    @use staticServer(path.join(__dirname, './assets'))

    @start()

  spawnBackend: (p, config)->
    cfg =
      script: p
      ext: 'js json coffee'
      env:
        PORT: 4000
    if config?
      helpers.extend cfg, config
    log cfg
    nodemon cfg

