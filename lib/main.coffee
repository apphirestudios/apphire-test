'use strict'
window.async = require('co')

window.throws = (fn, regexpOrErrorType)-> async ->
  error = undefined
  try
    yield fn
  catch e
    error = e
  if error?
    if regexpOrErrorType
      if regexpOrErrorType instanceof RegExp
        regexp = regexpOrErrorType
        if not regexp.test error.toString()
          throw new Error('Function should throw an error matching ' + (regexp or '') + ' but it returned ' + error.toString())
      else
        errorType = regexpOrErrorType
        if not(error instanceof errorType)
          throw new Error('Function should throw an error type ' + errorType.name + ' but it returned ' + error.name + ' : ' + error.toString())

  else
    throw new Error('Function should throw an error but it ended normally')

window.should = require('chai').should()
window.expect = require('chai').expect
window.log = console.log.bind @

window.prepareRunner = ->
  document.getElementById('mocha').innerHTML = ''
  delete window.mocha
  delete require.cache[require.resolve("mocha")]
  delete require.cache[require.resolve("co-mocha")]
  require 'mocha'
  require 'co-mocha'
  mocha.setup('bdd')

window.runTests = ()->
  mocha.run()

